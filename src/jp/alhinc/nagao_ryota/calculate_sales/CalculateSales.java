package jp.alhinc.nagao_ryota.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchName = new HashMap<>();
		Map<String, Long> sale = new HashMap<>();
		List<Integer> numberName = new ArrayList<>();

		BufferedReader br = null;
		try {
			File readFile = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(readFile);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				String[] code = line.split(",");

				if((!code[0].matches("^\\d{3}") || (code.length != 2))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//[0]:支店コード、[1]:支店名
				branchName.put(code[0], code[1]);
				//[0]:支店コード
				sale.put(code[0], (long) 0);

			}

		} catch (IOException e) {
			System.out.println("支店定義ファイルが存在しません");	
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");	
					return;
				}
			}
		}


		String path = args[0];
		File dir = new File(path);
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName().toString();
			if (!fileName.matches("^\\d{8}.rcd$")) {
				continue;
			}

			File rcdFile = new File(fileName);
			String rcdNameFile = rcdFile.getName().toString();
			String rcdNumberFile = rcdNameFile.substring(0,8);
			int rcdNumber = Integer.parseInt(rcdNumberFile);
			numberName.add(rcdNumber);

			Collections.sort(numberName);
			if(numberName.get(numberName.size() -1) - numberName.get(0) != numberName.size() -1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			BufferedReader br2 = null;
			try {
				File salefile = new File(args[0],rcdNameFile);
				FileReader fr2 = new FileReader(salefile);
				br2 = new BufferedReader(fr2);

				//支店コード
				String branchCode = br2.readLine();
				//売上金額
				String saleData = br2.readLine();
				//3行目エラー処理用
				String line3 = br2.readLine();

				if(!branchName.containsKey(branchCode)) {
					System.out.println(rcdNameFile + "の支店コードが不正です");
					return;
				}

				if(line3 != null) {
					System.out.println(rcdNameFile + "のフォーマットが不正です");
					return;
				}

				long data = Long.parseLong(saleData);
				sale.put(branchCode,data + sale.get(branchCode));

				if(!saleData.matches("^\\d{1,10}$")) {
					System.out.println("売上金額が10桁を超えてます");
					return;
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br2 != null) {
					try {
						br2.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}		
			}
		}

		BufferedWriter bw = null;
		try {
			File writefile = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(writefile);
			bw = new BufferedWriter(fw);

			for(String key : branchName.keySet()) {
				String branch = (key + "," + branchName.get(key) + "," + sale.get(key));
				bw.write(branch + "\r\n");

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}

